from datetime import datetime

from .models import Challenge, User, SolveTimeModel


class Leaderboard:
    def __init__(self):
        self._data = []
        self.points_per_challenge = {}
        self.is_easter = {}
        self.points_per_user = {}
        self.points_graph = []
        self.lbdata = []

        self.first_blood_count = {}
        self.extreme_count = {}
        self.easter_egg_count = {}

        self.firsts = {}

    def points_for(self, user):
        if isinstance(user, dict):
            return self.points_per_user.get(str(user['discord_user_id']), 0)
        return self.points_per_user.get(str(user.discord_user_id), 0)

    def get_first_bloods(self, user):
        if isinstance(user, dict):
            return self.first_blood_count.get(str(user['discord_user_id']), 0)
        return self.first_blood_count.get(str(user.discord_user_id), 0)

    def get_extreme_count(self, user):
        if isinstance(user, dict):
            return self.extreme_count.get(str(user['discord_user_id']), 0)
        return self.extreme_count.get(str(user.discord_user_id), 0)

    def get_easter_egg_count(self, user):
        if isinstance(user, dict):
            return self.easter_egg_count.get(str(user['discord_user_id']), 0)
        return self.easter_egg_count.get(str(user.discord_user_id), 0)

    def get_lbdata(self):
        return self.lbdata

    def get_first_solve(self, challenge):
        return self.firsts.get((challenge.category, challenge.challenge), None)

    def reload_lbdata(self):
        self.lbdata = []
        for user in User.query.all():
            self.lbdata.append(dict(
                display_name=user.display_name,
                points=self.points_for(user),
                isbanned=user.isbanned,
                isbeta=user.isbeta,
                discord_user_id=str(user.discord_user_id),

                first_bloods=self.get_first_bloods(user),
                extreme_count=self.get_extreme_count(user),
                easter_egg_count=self.get_easter_egg_count(user),
            ))
        self.lbdata.sort(key=lambda x: (
    -x['points'],
    (lambda x: 0 if x is None else x.solve_time)(SolveTimeModel.query.filter_by(user_id=x['discord_user_id']).order_by(SolveTimeModel.solve_time.desc()).first())
))

    def reload_ppc(self):
        self.points_per_challenge = {}
        for chal in Challenge.query.all():
            self.points_per_challenge[(chal.category, chal.challenge)] = chal.points
            self.is_easter[(chal.category, chal.challenge)] = chal.ishidden

    def reload_data(self):
        self._data = {}
        self.first_blood_count = {}
        self.extreme_count = {}
        self.easter_egg_count = {}

        first = {}

        for user in User.query.all():
            uid = str(user.discord_user_id)
            self._data[uid] = []
            for solve in SolveTimeModel.query.filter_by(user_id=user.discord_user_id).order_by(SolveTimeModel.solve_time.asc()).all():
                chal_id = (solve.category, solve.challenge)

                self._data[uid].append((
                    solve.solve_time,
                    self.points_per_challenge.get(chal_id, 0)
                ))

                if uid not in self.first_blood_count:
                    self.first_blood_count[uid] = 0
                    self.extreme_count[uid] = 0
                    self.easter_egg_count[uid] = 0

                if self._data[uid][-1][1] >= 900:
                    self.extreme_count[uid] += 1

                if self.is_easter[chal_id]:
                    self.easter_egg_count[uid] += 1

                if chal_id not in first or solve.solve_time < first[chal_id][0]:
                    first[chal_id] = (solve.solve_time, solve.user_id)

        self.firsts = {i: str(first[i][1]) for i in first}
        for i in self.firsts:
            if self.firsts[i] not in self.first_blood_count:
                self.first_blood_count[self.firsts[i]] = 0
            self.first_blood_count[self.firsts[i]] += 1

    def reload_pg(self):
        data = []

        for n, user in enumerate(User.query.all()):
            u_dat = self._data.get(str(user.discord_user_id), [])

            plot = {'x': [], 'y': [], 'name': str(user.display_name), 'mode': 'lines+markers'}
            points = 0
            mtime = 0

            for st, pt in u_dat:
                points += pt
                plot['x'].append(st.isoformat())
                plot['y'].append(points)

                mtime = max(mtime, (1<<32) - datetime.timestamp(st))
            # Sort by points, then inverse time, then a backup int to avoid sorting by a dict
            data.append((points, mtime, n, plot))

        data.sort(reverse=True)
        self.points_graph = [i[-1] for i in data[:15]]

    def reload_ppu(self):
        self.points_per_user = {
            i: sum([j[1] for j in self._data[i]])
            for i in self._data
        }

    def reload_all(self):
        self.reload_ppc()
        self.reload_data()
        self.reload_pg()
        self.reload_ppu()
        self.reload_lbdata()


leaderboard = Leaderboard()
