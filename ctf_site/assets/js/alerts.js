let ALERT_EXPIRE = 7200;  // 2 hours

document.addEventListener("DOMContentLoaded", function() {
    let socket = io.connect('/', {
        transports: ['websocket', 'polling'], rememberTransport: false, port: location.port});

    socket.on('alert', function(alert_data) {
        if (getCookie('alert_' + alert_data.uuid)) return;
        setCookieSeconds('alert_' + alert_data.uuid, 'hide', ALERT_EXPIRE);
        createPopup(alert_data["title"], alert_data["content"]);
    });

    socket.on('flag_score', function(alert_data) {
        createScorePopup(alert_data["username"], alert_data["challenge_name"], alert_data["isFirst"])
    });

    socket.on('donate', function(alert_data) {
        createDonationPopup(alert_data["nickname"], alert_data["donation_amount"], alert_data["anonymous"])
    });

    socket.on('complete', function(alert_data) {
        createCompletePopup(alert_data["username"])
    });

    socket.on('leaderboard', function(lbdata) {
        let el = $("#leader-body");
        if (!el.length) return;
        el.html("");

        /* See leaderboard.html for a nicer visualisation of
           what this is actually doing. */
        for (let i = 0; i < lbdata.length; i++) {
            let user = lbdata[i];
            let wrap = $("<a>");
            wrap.attr("href", "/user/" + user.display_name);

            let uel = $("<div class='lb-item'>");
            let ttl = $("<div class='lbi-title'>");
            ttl.text(user.display_name);
            if (user.isbanned)
                ttl.addClass('lbi-title-banned');
            uel.append(ttl);
            let pts = $("<div class='lbi-points'>");
            pts.append(
                $("<div class='lbi-points-t'>").text(user.points + " points")
            );
            if (user.first_bloods !== 0 || user.extreme_count !== 0 || user.easter_egg_count !== 0 || user.isbeta) {
                let lbb = $("<div class='lbi-points-b'>");
                if (user.isbeta)
                    lbb.append($("<img class='beta' src='/static/img/beta.png'>"));
                for (let j = 0; j < user.easter_egg_count; j++)
                    lbb.append($("<img src='/static/img/egg.png'>"));
                for (let j = 0; j < user.extreme_count; j++)
                    lbb.append($("<img src='/static/img/skull.png'>"));
                for (let j = 0; j < user.first_bloods; j++)
                    lbb.append($("<img src='/static/img/firstsolve.png'>"));
                pts.append(lbb);
            }
            uel.append(pts);
            wrap.append(uel);
            el.append(wrap);
        }
    });
});
