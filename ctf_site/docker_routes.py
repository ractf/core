import docker
from flask import render_template, abort, Blueprint

from .util.decorators import admin_only
from .util.limiter import limiter

docker_routes = Blueprint(
    "docker_routes", __name__, template_folder="templates", url_prefix="/docker"
)
client = docker.from_env()

limiter.limit("1/second")(docker_routes)


def sizeof_fmt(num, suffix="B"):
    for unit in ["", "k", "M", "G", "T", "P", "E", "Z"]:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, "Yi", suffix)


@docker_routes.route("/")
@admin_only
def docker_index():
    return render_template("docker/index.html", containers=client.containers.list())


@docker_routes.route("/logs/<container>")
@admin_only
def docker_logs(container):
    try:
        container = client.containers.get(container)
    except docker.errors.NotFound:
        return abort(404)

    tag = container.image.tags[0].split(":")[0]
    return render_template("docker/logs.html", tag=tag, name=container.name)


@docker_routes.route("/stop/<container>")
@admin_only
def docker_stop_container(container):
    try:
        container = client.containers.get(container)
    except docker.errors.NotFound:
        return abort(404)

    container.stop()
    container.remove()

    return 'Container stopped and deleted<br><br><a href="/docker/">Back</a>'


@docker_routes.before_request
def before_request():
    try:
        client.ping()
    except Exception as e:
        return render_template("pages/error.html", msg=f"Docker not running or permissions insufficient.")
