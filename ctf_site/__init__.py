import traceback
import time

from flask import Flask, jsonify, redirect, render_template, request, url_for
from werkzeug.exceptions import HTTPException
from werkzeug.routing import RequestRedirect
from flask_assets import Environment, Bundle

from .challenge_editor_routes import editor_routes
from .challenge_populator import populate_challenges
from .docker_routes import docker_routes
from .api_routes import api, ipn
from .util.limiter import limiter
from .util.paths import resolve_path
from .api_routes import api, ipn
from .webhooks import send_webhook
from .config import Config, countdown
from .models import db, User
from .server import ctf_page, socketio
from .csrf import csrf
from .support_routes import support
from .leaderboard import leaderboard

app = Flask(
    __name__, static_url_path="", static_folder="static", template_folder="templates"
)

assets = Environment(app)
assets.url = app.static_url_path
assets.directory = app.static_folder
assets.append_path(resolve_path('assets'))

scss = Bundle('bundle.scss', filters='cssmin,pyscss', depends='**/*.scss', output='css/bundle.css')
assets.register('scss_all', scss)
scss = Bundle('formal.scss', filters='cssmin,pyscss', depends='**/*.scss', output='css/formal.css')
assets.register('scss_formal', scss)

js = Bundle(
    'js/*/*.js', 'js/*.js',
    filters='jsmin', output='js/packed.js'
)
assets.register('js_all', js)

js = Bundle(
    'js/vendor/*.js', 'js/cookies.js',
    filters='jsmin', output='js/bare.js'
)
assets.register('js_bare', js)

app.config.update(Config.__dict__)
app.register_blueprint(api)
app.register_blueprint(ipn)
app.register_blueprint(docker_routes)
app.register_blueprint(editor_routes)
app.register_blueprint(ctf_page)
app.register_blueprint(support)

db.init_app(app)
app.app_context().push()
db.create_all()
populate_challenges()
csrf.init_app(app)  # I'm sorry Dave, but I'm afraid I can't do that.
limiter.init_app(app)
socketio.init_app(app)

leaderboard.reload_all()

@app.errorhandler(Exception)
def handle_error(e):
    code = 500
    # Not found, unauthorized, etc.
    if isinstance(e, HTTPException):
        code = e.code

    # HTTP 3xx codes aren't errors
    if isinstance(e, RequestRedirect):
        return redirect(e.new_url), code

    # Only the API uses CSRF tokens
    if "CSRF" in str(e):
        return jsonify({"s": False, "m": "CSRF is missing or invalid"})

    # API rate limits
    if code == 429:
        return jsonify({"s": False, "m": "You are sending too many requests to the API"})

    # Show a nice error page
    if code == 500:
        traceback.print_exc()

        exc = traceback.format_exc()
        # 2000 char limit. Strip start because most-recent-call-last
        exc = exc[-1990:]
        send_webhook("Page Handler",
            f"Error in page view: `{request.path}`",
            f"```py\n{exc}\n```")
    return render_template("pages/error.html", msg=e), code


@app.before_request
def before_request():
    COUNTDOWN_WHITELIST = [
        "countdown",  # Let's avoid redirect loops :)
        "privacy",
        "conduct",
        "about",
        "",
        "contributors"
    ]

    # Check the date
    if time.time() > countdown:
        return

    # Things like JS and CSS will have a `.` in the URL
    if "." in request.path and not request.path.endswith('.txt'):
        return

    # Whitelist for sites we want to allow access to at all times
    if request.path.strip('/').split('/')[0] in COUNTDOWN_WHITELIST:
        return

    # Allow logged in users access regardless
    token = request.cookies.get("token")
    data = User.query.filter_by(auth_token=token).first()
    if data:
        return None

    # Bar entry to the site!
    return redirect(url_for("ctf_page.countdown_route"))
