import json
import os

from .models import challenge_categories_rosetta, db, Challenge

flag_type_indexes = {
    "text": "some_text",
    "f-leet": "cdctf{l33tsp3ak_fl4g}",
    "sentence": "Some words in a sentence",
    "leet": "l33t_sp34k",
    "f-text-upper": "cdctf{UPPERCASE_TEXT}",
    "f-text": "cdctf{TextHere}",
    "random": "RANDOM DATA",
    "email": "someone@something.tld",
    "f-random": "cdctf{RANDOMDATA}"
}


def populate_challenges():
    with open(f"{os.path.dirname(__file__)}/../challenges/challenges.json") as json_file:
        challenge_data = json.load(json_file)

    categories = challenge_categories_rosetta.keys()
    added = modified = 0
    for category in categories:
        for challenge in challenge_data[category]:
            challenge["description"] = challenge["description"]

            challenge_number = challenge["number"]
            existing_challenge = Challenge.query.filter_by(
                category=category, challenge=challenge_number
            ).first()

            if existing_challenge is not None:
                original = dict(existing_challenge.__dict__)
                existing_challenge.title = challenge["title"]
                existing_challenge.description = challenge["description"]
                existing_challenge.flag = challenge["flagHash"]
                existing_challenge.points = challenge["points"]
                existing_challenge.hint = challenge["hint"]
                existing_challenge.ishidden = challenge.get("hidden", False)

                if any(original[i] != getattr(existing_challenge, i) for i in original):
                    modified += 1
                    db.session.commit()
            else:
                new_challenge = Challenge(
                    category=category,
                    challenge=challenge_number,
                    title=challenge["title"],
                    description=challenge["description"],
                    flag=challenge["flagHash"],
                    points=challenge["points"],
                    hint=challenge["hint"],
                    ishidden=challenge.get("hidden", False),
                    flag_format=flag_type_indexes[challenge["flag_format_code"]]
                )
                db.session.add(new_challenge)
                db.session.commit()
                added += 1

    if added:
        print(f'--> Added {added} new challenge{"s" if added != 1 else ""}')
    if modified:
        print(f'--> Modified {modified} challenge{"s" if modified != 1 else ""}')
    if not (added or modified):
        print('--> Challenge database already up to date')


if __name__ == "__main__":
    populate_challenges()
