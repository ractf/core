import os
import uuid
from json import dump, load

from flask import Blueprint, jsonify, render_template, request
from flask_socketio import emit

from .util.decorators import admin_only
from .util.paths import resolve_path
from .webhooks import send_webhook
from .models import Challenge, challenge_categories_rosetta, db
from .api_routes import alerts, news

editor_routes = Blueprint(
    "challenge_editor",
    __name__,
    template_folder="templates",
    url_prefix="/admin/challenge_editor",
)


@editor_routes.route("/")
@admin_only
def docker_index():
    return render_template(
        "challenge_editor/index.html", categories=challenge_categories_rosetta
    )


@editor_routes.route("/<category>")
@admin_only
def view_challenges(category):
    challenges = Challenge.query.filter_by(category=category).all()

    return render_template(
        "challenge_editor/view_challenges.html",
        category=category,
        challenges=challenges,
        category_name=challenge_categories_rosetta[category],
    )


@editor_routes.route("/<category>/<challenge_num>")
@admin_only
def edit_challenge(category, challenge_num):
    challenge = Challenge.query.filter_by(
        category=category, challenge=challenge_num
    ).first()
    if not challenge:
        return render_template("pages/error.html", msg="Challenge does not exist")
    else:
        path = resolve_path("../challenges/challenges.json")
        with open(path, "r") as f:
            challenges = load(f)
        flagformat = challenges[category][int(challenge_num) - 1]["flag_format_code"]
        details = {
            "title": challenge.title,
            "description": challenge.description,
            "points": challenge.points,
            "flag_hash": challenge.flag,
            "hint": challenge.hint,
            "format": flagformat,
        }

    return render_template(
        "challenge_editor/edit_challenge.html",
        category=category,
        category_name=challenge_categories_rosetta[category],
        challenge=challenge,
        **details,
    )


def reload_challenge(category, challenge_num):
    """Reloads the specified challenge in the database, applying any new values from challenges.json"""
    existing_challenge = Challenge.query.filter_by(
        category=category, challenge=challenge_num
    ).first()
    newPointValue = None

    path = resolve_path("../challenges/challenges.json")
    with open(path, "r") as f:
        challenges = load(f)

    new_values = challenges[category][challenge_num - 1]
    new_points = int(new_values["points"])
    if new_points != existing_challenge.points:
        newPointValue = new_points

    existing_challenge.title = new_values["title"]
    existing_challenge.description = new_values["description"]
    existing_challenge.flag = new_values["flagHash"]
    existing_challenge.points = int(new_values["points"])
    existing_challenge.hint = new_values["hint"]
    existing_challenge.flag_format = new_values["flag_format_code"]

    db.session.commit()
    print("Reloaded Challenge")
    return newPointValue


@editor_routes.route("/save/<category>/<int:challenge_num>", methods=["POST"])
@admin_only
def save_configuration(category, challenge_num):
    data = request.form

    path = resolve_path("../challenges/challenges.json")

    with open(path, "r") as f:
        challenges = load(f)

    new_challenge = {
        "title": data["title"],
        "number": challenge_num,
        "description": data["description"],
        "hint": data["hint"],
        "flagHash": data["flag_hash"],
        "points": int(data["points"]),
        "flag_format_code": data["format"],
    }
    changes = []
    for i in challenges[category][challenge_num - 1]:
        if challenges[category][challenge_num - 1][i] != new_challenge[i]:
            changes.append((i, challenges[category][challenge_num - 1][i], new_challenge[i]))

    challenges[category][challenge_num - 1] = new_challenge

    path = resolve_path("../challenges/challenges.json")
    with open(path, "w") as f:
        dump(challenges, f, sort_keys=True, indent=4)

    newPointValue = reload_challenge(category, challenge_num)
    if newPointValue is not None:
        alert_data = {
            "title": f"Challenge Points changed for {data['title']}!",
            "content": f"It is now worth {newPointValue} points.",
            "uuid": uuid.uuid4().hex,
        }
        emit("alert", alert_data, namespace="/", broadcast=True)
        alerts.append(alert_data)
        news.append(alert_data)

    if changes:
        fields = []
        for i in changes:
            orig = '\n'.join('- ' + j for j in str(i[1]).split('\n'))
            new = '\n'.join('+ ' + j for j in str(i[2]).split('\n'))
            fields.append(dict(
                name=i[0],
                inline=True,
                value=f'```diff\n{orig}\n{new}\n```'
            ))
        send_webhook(
            "Challenge Editor",
            f"Challenge {category}/{challenge_num} Updated",
            fields=fields
        )

    return jsonify({"s": True, "m": "Challenge updated successfully."})
